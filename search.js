function buttonClick() {
    document.getElementById("printOutput").innerHTML = "";
    var input = document.getElementById("getInput").value;
    var regex = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g;
    input = input.replace(regex, ' ');
    var url = "https://api.bing.microsoft.com/v7.0/custom/search?q=" + encodeURIComponent(input) + "&customconfig=6dde9156-25d7-430a-9d15-4c052db2f002&mkt=en-US";
    const xhr = new XMLHttpRequest();

    xhr.onload = function(){
        const res = JSON.parse(this.responseText);
        const result = res.webPages.value;
        for (i=0; i<result.length; i++){
            var outputLink = document.createElement('a');
            outputLink.setAttribute('href',result[i].url);
            outputLink.innerHTML = "<br/>" + result[i].name;
            outputLink.style.fontSize = "24px";
            var outputImg = document.createElement("img");
            outputImg.src = result[i].openGraphImage.contentUrl;
            outputImg.style.width = "100px";
            outputImg.setAttribute("align", "left");
            var outputDiv = document.createElement("P");
            outputDiv.innerHTML = result[i].snippet;
            document.getElementById("printOutput").appendChild(outputLink);
            document.getElementById("printOutput").appendChild(outputImg);
            document.getElementById("printOutput").appendChild(outputDiv);
        }
    }
    
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Ocp-Apim-Subscription-Key", "bf7a8c02645b48579d9ce18525d0eaa1");
    xhr.send();
}