const filter = (upto) => {
    var regex = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g;
    return upto.replace(regex, ' ');
}

console.log(filter("hello world"));
console.log(filter("hello%world"));
console.log(filter("!hello world"));
console.log(filter(" hello world "));
console.log(filter("hello;world"));
console.log(filter("hello world?"));

module.exports = filter;